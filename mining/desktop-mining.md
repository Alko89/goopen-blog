# Mine on your Linux desktop when screen is locked

If you just bought a fancy new GPU and want to make the most use of it, why not use its idle time to mine some coins? In this example we will start the miner when the screen locks up using a simple Bash script and Systemd user service.

## Bash  script

```
#!/bin/bash

running=false
pid=-1

dbus-monitor --session "type='signal',interface='org.freedesktop.ScreenSaver'" |
  while read x; do
    case "$x" in
      *"boolean true"*)
        if [[ $running = false ]] ; then
            # Replace POOL_URL, ADDRESS and NAME.
            ~/Mining/nevermore-miner/ccminer -a x16r -a x16r -o POOL_URL -u ADDRESS -p NAME &
            pid=$!
            running=true
            echo "pid is $pid"
        fi;;
      *"boolean false"*)
        if [[ $running = true && $pid > 0 ]] ; then
            echo "killing $pid..."
            kill -9 $pid
            pid=-1
            running=false
        fi;;
    esac
  done
```

Save the script and make it executable `chmod 755 ccrunner.sh`.

## Create start and enable the user systemd service

```
[Unit]
Description=Ravencoin (RVN) miner

[Service]
Type=simple
ExecStart=~/Mining/ccrunner.sh

[Install]
WantedBy=default.target
```

Save the service file to `~/.config/systemd/user/` folder.

Start service `systemctl --user start ccminer-sandbox.service`.

Enable service on login `systemctl --user enable ccminer-sandbox.service`.
