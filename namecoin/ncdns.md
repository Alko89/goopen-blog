# NCDNS

Namecoin to DNS bridge daemon

Download [ncdns](https://github.com/namecoin/ncdns/releases) daemon or build it yourself. Next create a config file `etc/ncdns.conf` using RPC settings set in the Namecoin daemon.

```
### The address, in "hostname:port" format, of the Namecoin JSON-RPC interface.
namecoinrpcaddress="127.0.0.1:8336"

### The username with which to connect to the Namecoin JSON-RPC interface.
namecoinrpcusername="namecoinrpc"

### The password with which to connect to the Namecoin JSON-RPC interface.
namecoinrpcpassword="rpcpass"
```

*To allow ncdns to open port 53 you must bind a socket to Internet domain privileged port (port numbers less than 1024) using setcap (`sudo setcap 'cap_net_bind_service=+ep' ./ncdns`)*

## Systemd

Write a systemd service to enable NCDNS to start at boot. You can also write a service for `namecoind`.

```
[Unit]
Description=NCDNS - Namecoin to DNS bridge daemon
After=network.target

[Service]
User=USERNAME
WorkingDirectory=~/ncdns/bin/
ExecStart=~/ncdns/bin/ncdns

[Install]
WantedBy=default.target
```

Save the service file to `/etc/systemd/system/ncdns.service` folder.

Reload systemd daemon `sudo systemctl daemon-reload`

Start service `sudo systemctl start ncdns.service`.

Enable service to start at boot  `sudo systemctl enable ncdns.service`.

To see if the service has started without errors, you can use `systemctl status ncdns.service` to check if the service is running.
